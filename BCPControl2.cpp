//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("BCPControlForm_unit.cpp", BCPControlForm);
USEFORM("SetupForm_unit.cpp", SetupForm);
USEFORM("SelectVarForm_unit.cpp", SelectVarForm);
USEFORM("CalculatePlenumPressForm_unit.cpp", CalculatePlenumPressForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TBCPControlForm), &BCPControlForm);
       Application->CreateForm(__classid(TSetupForm), &SetupForm);
       Application->CreateForm(__classid(TSelectVarForm), &SelectVarForm);
       Application->CreateForm(__classid(TCalculatePlenumPressForm), &CalculatePlenumPressForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   catch (...)
   {
       try
       {
          throw Exception("");
       }
       catch (Exception &exception)
       {
          Application->ShowException(&exception);
       }
   }
   return 0;
}
//---------------------------------------------------------------------------

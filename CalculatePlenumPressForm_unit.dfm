object CalculatePlenumPressForm: TCalculatePlenumPressForm
  Left = 280
  Top = 183
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1095#1080#1089#1083#1077#1085#1080#1077' '#1076#1072#1074#1083#1077#1085#1080#1103' '#1074' '#1087#1085#1077#1074#1084#1086#1089#1080#1089#1090#1077#1084#1077
  ClientHeight = 53
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 50
    Top = 2
    Width = 5
    Height = 24
    Caption = '/'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 99
    Top = 2
    Width = 11
    Height = 24
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 165
    Top = 2
    Width = 11
    Height = 24
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 181
    Top = 6
    Width = 53
    Height = 16
    Caption = #1054#1096#1080#1073#1082#1072'!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 376
    Top = 10
    Width = 23
    Height = 13
    Caption = #1052#1055#1072
  end
  object Label6: TLabel
    Left = 367
    Top = 30
    Width = 47
    Height = 16
    Caption = #1082#1075#1089'/'#1089#1084'2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 6
    Top = 30
    Width = 341
    Height = 13
    AutoSize = False
    Caption = #1054#1096#1080#1073#1082#1072'!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object NumberEdit1: TNumberEdit
    Left = 1
    Top = 2
    Width = 47
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
  object NumberEdit2: TNumberEdit
    Left = 56
    Top = 2
    Width = 43
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
  object NumberEdit3: TNumberEdit
    Left = 114
    Top = 2
    Width = 48
    Height = 24
    BevelInner = bvNone
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    OnChange = NumberEdit1Change
    NumberType = Double
  end
end

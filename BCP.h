//---------------------------------------------------------------------------

#ifndef BCPH
#define BCPH
//---------------------------------------------------------------------------
#include <vcl.h>

//---------------------------------------------------------------------------

struct TUsedVars
   {
   bool t;
   bool P;
   bool Dens;
   bool Period;
   bool Time;
   bool Time2;
   bool N;
   bool NN;
   bool Q;
   bool Q3;
   bool V_etalon;
   bool V_work;
   TUsedVars()
      {
      t=false;
      P=false;
      Dens=false;
      Period=false;
      Time=false;
      Time2=false;
      N=false;
      NN=false;
      Q=false;
      Q3=false;
      V_etalon=false;
      V_work=false;
      }
   };
//---------------------------------------------------------------------------
struct TProveData
   {
   double t;
   double P;
   double Dens;
   double Period;
   double Time;
   double Time2;
   int N;
   double NN;
   double Q;
   double Q3;
   double V_etalon;
   double V_work;
public:
   AnsiString __fastcall DataToString(TUsedVars* UsedVars);
   void __fastcall DataFromString(AnsiString s, TUsedVars* UsedVars);
   };
//---------------------------------------------------------------------------


#endif



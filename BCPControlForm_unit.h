//---------------------------------------------------------------------------

#ifndef BCPControlForm_unitH
#define BCPControlForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "modbus.h"
#include <ExtCtrls.hpp>
#include "CSPIN.h"
#include "BCP.h"

const MaxAverageRuns=10;

//---------------------------------------------------------------------------
class TBCPControlForm : public TForm
{
__published:	// IDE-managed Components
   TPageControl *PageControl1;
   TTabSheet *TabSheet1;
   TLabel *Label3;
   TGroupBox *GroupBox1;
   TLabel *Label8;
   TLabel *Label9;
   TLabel *Label12;
   TLabel *QLabel;
   TLabel *DLabel;
   TLabel *NLabel;
   TLabel *Label13;
   TLabel *Q3Label;
   TLabel *Label10;
   TLabel *Label11;
   TButton *SetupProveDataButton;
   TTimer *Timer1;
   TLabel *tLabel;
   TLabel *PLabel;
   TButton *GoProveButton;
   TLabel *StateLabel;
   TLabel *NNLabel;
   TButton *Button1;
   TTabSheet *TabSheet2;
   TGroupBox *GroupBox2;
   TLabel *Label17;
   TLabel *Label21;
   TLabel *Label22;
   TLabel *Q2Label;
   TLabel *D2Label;
   TLabel *N2Label;
   TLabel *Label26;
   TLabel *Q32Label;
   TLabel *Label28;
   TLabel *Label29;
   TLabel *t22Label;
   TLabel *P2Label;
   TLabel *V1Label;
   TLabel *Label35;
   TLabel *V2Label;
   TLabel *Label37;
   TLabel *Label16;
   TLabel *Label32;
   TLabel *T1Label;
   TLabel *T2Label;
   TLabel *Label1;
   TComboBox *ComboBox1;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *UpstreamLabel;
   TLabel *RunLabel;
   TLabel *Label2;
   TCSpinEdit *PointNumberEdit;
   TLabel *Label14;
   TCSpinEdit *TotalRunEdit;
   TLabel *Label18;
   TCSpinEdit *AverageRunEdit;
   TButton *Button2;
   TMemo *Memo1;
   TButton *SelectVarButton;
   TLabel *PeriodLabel;
   TLabel *Period2Label;
   TLabel *Label15;
   TButton *SendCurrentDataButton;
   TLabel *DDELabel;
   TLabel *dpLabel;
   void __fastcall ComboBox1Change(TObject *Sender);
   void __fastcall SetupProveDataButtonClick(TObject *Sender);
   void __fastcall OnTimer(TObject *Sender);
   void __fastcall GoProveButtonClick(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall SelectVarButtonClick(TObject *Sender);
   void __fastcall SendCurrentDataButtonClick(TObject *Sender);
public:		// User declarations


private:	// User declarations
   double DensK[9];
   double dens_t;
   double dens_p;
   double CalculateDensity(double Period,double t,double P);
   TDDEServer Server;
   void __fastcall OnLoop(TDDEServer *Sender);
   TModbus* mb;
   TModbusPoint* PRType;
   TModbusPoint* K;
   TModbusPoint* V1;
   TModbusPoint* V2;
   TModbusPoint* Q;
   TModbusPoint* Q3;
   TModbusPoint* t;
   TModbusPoint* P;
   TModbusPoint* D;
   TModbusPoint* N;
   TModbusPoint* T1;
   TModbusPoint* NN;
   TModbusPoint* Prove;
   TModbusPoint* ProveEndOk;
   TModbusPoint* ProveEndError;
   TModbusPoint* Run;
   TModbusPoint* Upstream;
   TModbusPoint* Period;

   TModbusPoint* CustomPacket;

   short sh;
   float f;
   double pp;
   void __fastcall OnSimpleRunEnd();
   TProveData CurrentRun; //������� ������ ����� ���������� ���������
   TProveData CurrentData;//������ ������� ������� ������
   TProveData Runs[MaxAverageRuns+1];
   int CurrentTotalRun;
   int CurrentAverageRun;

   int TotalRuns;
   int AverageRuns;
   void __fastcall GoProve();
   void __fastcall CalculateAverage();
   void __fastcall Log(AnsiString Msg);
   void __fastcall ClearReadedList();
   bool __fastcall CheckForReaded();
   void __fastcall SendData(TProveData* Data, TUsedVars* Used);
   void __fastcall CopyDataFromCustomToPoint(char* From, TModbusPoint* To, int size,int count=1);
//������ 413757
   enum
      {WriteCommandStart=0,ProveInProgres=1,/*ProveCompletedOk=2,*/NeedReadData,
      ReadData,SimpleRunEnd,ProveBreak,ProveError,ProveNone}ProveState;
public:		// User declarations
   __fastcall TBCPControlForm(TComponent* Owner);
   __fastcall ~TBCPControlForm();
   void __fastcall OnCommBreak();
   void __fastcall OnCommOk();
   void __fastcall OnTypeComm();
   void __fastcall OnKComm();
   void __fastcall OnQComm();
   void __fastcall OnCommQ3();
   void __fastcall OnCommt();
   void __fastcall OnCommP();
   void __fastcall OnCommD();
   void __fastcall OnCommN();
   void __fastcall OnCommNN();
   void __fastcall OnProve();
   void __fastcall OnProveOk();
   void __fastcall OnProveError();
   void __fastcall OnCommV1();
   void __fastcall OnCommV2();
   void __fastcall OnCommT1();
   void __fastcall OnCommUp();
   void __fastcall OnCommRun();
   void __fastcall OnCommPeriod();
   void __fastcall OnCommCustom();

   TUsedVars UsedVars;
   TUsedVars ReadedVars;
};
//---------------------------------------------------------------------------
extern PACKAGE TBCPControlForm *BCPControlForm;
//---------------------------------------------------------------------------
#endif

//---------------------------------------------------------------------------

#ifndef SetupForm_unitH
#define SetupForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>

#include "Modbus.h"
//---------------------------------------------------------------------------
class TSetupForm : public TForm
{
__published:	// IDE-managed Components
   TRadioGroup *RadioGroup2;
   TGroupBox *GroupBox1;
   TComboBox *ComboBox1;
   TLabel *Label4;
   TNumberEdit *NumberEdit1;
   TLabel *Label2;
   TRadioButton *RadioButton1;
   TRadioButton *RadioButton2;
   TLabel *Label1;
   TNumberEdit *NumberEdit2;
   TGroupBox *GroupBox2;
   TLabel *Label3;
   TLabel *Label5;
   TComboBox *ComboBox2;
   TNumberEdit *NumberEdit3;
   TRadioButton *RadioButton3;
   TRadioButton *RadioButton4;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
   void __fastcall ComboBox1Change(TObject *Sender);
   void __fastcall BitBtn1Click(TObject *Sender);
private:	// User declarations
   TModbusPoint* K1;
   TModbusPoint* K2;
   TModbusPoint* Type1;
   TModbusPoint* Type2;
   TModbusPoint* ProverType;
   TModbusPoint* V;
   bool Write[6];
   TModbus* mb;
public:		// User declarations
   __fastcall TSetupForm(TComponent* Owner);
   void __fastcall Setup(TModbus* mb);
   void __fastcall OnK1();
   void __fastcall OnK2();
   void __fastcall OnType1();
   void __fastcall OnType2();
   void __fastcall OnProverType();
   void __fastcall OnV();
   __fastcall ~TSetupForm();
};
//---------------------------------------------------------------------------
extern PACKAGE TSetupForm *SetupForm;
//---------------------------------------------------------------------------
#endif

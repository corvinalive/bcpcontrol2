//---------------------------------------------------------------------------


#pragma hdrstop

#include "BCP.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
AnsiString __fastcall TProveData::DataToString(TUsedVars* UsedVars)
{
   AnsiString s;
   s+=t;
   s+='\n';
   s+=P;
   s+='\n';
   s+=Dens;
   s+='\n';
   s+=Period;
   s+='\n';
   s+=Time;
   s+='\n';
   s+=Time2;
   s+='\n';
   s+=N;
   s+='\n';
   s+=NN;
   s+='\n';
   s+=Q;
   s+='\n';
   s+=Q3;
   s+='\n';
   s+=V_etalon;
   s+='\n';
   s+=V_work;
   s+='\n';
   if(UsedVars->t)
      {
      s+="1\n";
      }
   else
      {
      s+="0\n";
      }

   if(UsedVars->P)
      {
      s+="1\n";
      }
   else
      {
      s+="0\n";
      }

   if(UsedVars->Dens)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->Period)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->Time)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->Time2)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->N)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->NN)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->Q)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->Q3)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->V_etalon)
      s+="1\n";
   else
      s+="0\n";

   if(UsedVars->V_work)
      s+="1\n";
   else
      s+="0";

   return s;
}
//---------------------------------------------------------------------------
void __fastcall TProveData::DataFromString(AnsiString s, TUsedVars* UsedVars)
{
   const LinesCount=24;
   TStringList* ss=new TStringList();
   ss->Text=s;
   if(ss->Count!=LinesCount)
      {
      delete ss;
      return;
      }

   t=ss->Strings[0].ToDouble();
   P=ss->Strings[1].ToDouble();
   Dens=ss->Strings[2].ToDouble();
   Period=ss->Strings[3].ToDouble();
   Time=ss->Strings[4].ToDouble();
   Time2=ss->Strings[5].ToDouble();
   N=ss->Strings[6].ToInt();
   NN=ss->Strings[7].ToDouble();
   Q=ss->Strings[8].ToDouble();
   Q3=ss->Strings[9].ToDouble();
   V_etalon=ss->Strings[10].ToDouble();
   V_work=ss->Strings[11].ToDouble();
   if(UsedVars==NULL) return;
   UsedVars->t=(ss->Strings[12].ToInt()!=0);
   UsedVars->P=(ss->Strings[13].ToInt()!=0);
   UsedVars->Dens=(ss->Strings[14].ToInt()!=0);
   UsedVars->Period=(ss->Strings[15].ToInt()!=0);
   UsedVars->Time=(ss->Strings[16].ToInt()!=0);
   UsedVars->Time2=(ss->Strings[17].ToInt()!=0);
   UsedVars->N=(ss->Strings[18].ToInt()!=0);
   UsedVars->NN=(ss->Strings[19].ToInt()!=0);
   UsedVars->Q=(ss->Strings[20].ToInt()!=0);
   UsedVars->Q3=(ss->Strings[21].ToInt()!=0);
   UsedVars->V_etalon=(ss->Strings[22].ToInt()!=0);
   UsedVars->V_work=(ss->Strings[23].ToInt()!=0);
   delete ss;
}
//---------------------------------------------------------------------------
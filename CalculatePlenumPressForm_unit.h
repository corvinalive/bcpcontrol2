//---------------------------------------------------------------------------

#ifndef CalculatePlenumPressForm_unitH
#define CalculatePlenumPressForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
//---------------------------------------------------------------------------
class TCalculatePlenumPressForm : public TForm
{
__published:	// IDE-managed Components
   TNumberEdit *NumberEdit1;
   TLabel *Label1;
   TNumberEdit *NumberEdit2;
   TLabel *Label2;
   TNumberEdit *NumberEdit3;
   TLabel *Label3;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   void __fastcall NumberEdit1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TCalculatePlenumPressForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCalculatePlenumPressForm *CalculatePlenumPressForm;
//---------------------------------------------------------------------------
#endif

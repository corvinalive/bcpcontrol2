//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SetupForm_unit.h"
#include "Modbus.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TSetupForm *SetupForm;
//---------------------------------------------------------------------------
__fastcall TSetupForm::TSetupForm(TComponent* Owner)
   : TForm(Owner)
{
   Write[0]=false;
   Write[1]=false;
   Write[2]=false;
   Write[3]=false;
   Write[4]=false;
   Write[5]=false;
   K1 = new TModbusPoint(0,TModbusPoint::float32,&OnK1);
   K1->AutoRead=false;

   K2 = new TModbusPoint(17801,TModbusPoint::float32,&OnK2);
   K2->AutoRead=false;

   Type1 = new TModbusPoint(0,TModbusPoint::int16,&OnType1);
   Type1->AutoRead=false;

   Type2 = new TModbusPoint(13292,TModbusPoint::int16,&OnType2);
   Type2->AutoRead=false;

   ProverType = new TModbusPoint(3921,TModbusPoint::int16,&OnProverType);
   ProverType->AutoRead=false;

   V = new TModbusPoint(7919,TModbusPoint::float32,&OnV);
   V->AutoRead=false;
}
//---------------------------------------------------------------------------
/*
3921 ���������������� ��� ��������������� ������
0=����������������;
1=���������������;
2=���������������� - ��������������;
3=��������������� - SVP;
4=��������� ������;
5=��� ��������������� ���������� ��������������� �������.
*/
//---------------------------------------------------------------------------
void __fastcall TSetupForm::Setup(TModbus* mb_)
{
   RadioGroup2->Font->Color=clBlack;
   NumberEdit1->Font->Color=clBlack;
   NumberEdit2->Font->Color=clBlack;
   NumberEdit3->Font->Color=clBlack;
   Label2->Font->Color=clBlack;
   Label5->Font->Color=clBlack;
   mb=mb_;
   mb->AddNeedRead(K1);
   mb->AddNeedRead(K2);
   mb->AddNeedRead(Type1);
   mb->AddNeedRead(Type2);
   mb->AddNeedRead(ProverType);
   mb->AddNeedRead(V);
   ComboBox1Change(0);
   K2->Read();
   Type2->Read();
   ProverType->Read();
   V->Read();
   ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnK1()
{
   if(Write[0])
      {
      Write[0]=false;
      K1->Read();
      return;
      }
   NumberEdit1->Text=K1->Float[0];
   if(K1->State==TModbusPoint::CommOk)
      NumberEdit1->Font->Color=clBlue;
   else
      NumberEdit1->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnK2()
{
   if(Write[1])
      {
      Write[1]=false;
      K2->Read();
      return;
      }
   NumberEdit3->Text=K2->Float[0];
   if(K2->State==TModbusPoint::CommOk)
      NumberEdit3->Font->Color=clBlue;
   else
      NumberEdit3->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnType1()
{
   if(Write[2])
      {
      Write[2]=false;
      Type1->Read();
      return;
      }
   RadioButton1->Checked=Type1->Int16[0];
   RadioButton2->Checked=!RadioButton1->Checked;
   if(Type1->State==TModbusPoint::CommOk)
      Label2->Font->Color=clBlue;
   else
      Label2->Font->Color=clRed;}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnType2()
{
   if(Write[3])
      {
      Write[3]=false;
      Type2->Read();
      return;
      }
   RadioButton3->Checked=Type2->Int16[0];
   RadioButton4->Checked=!RadioButton3->Checked;
   if(Type2->State==TModbusPoint::CommOk)
      Label5->Font->Color=clBlue;
   else
      Label5->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnProverType()
{
   if(Write[4])
      {
      Write[4]=false;
      ProverType->Read();
      return;
      }
   RadioGroup2->ItemIndex=ProverType->Int16[0];
   if(ProverType->State==TModbusPoint::CommOk)
      RadioGroup2->Font->Color=clBlue;
   else
      RadioGroup2->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::OnV()
{
   if(Write[5])
      {
      Write[5]=false;
      V->Read();
      return;
      }
   NumberEdit2->Text=V->Float[0];
   if(V->State==TModbusPoint::CommOk)
      NumberEdit2->Font->Color=clBlue;
   else
      NumberEdit2->Font->Color=clRed;
}
//---------------------------------------------------------------------------
__fastcall TSetupForm::~TSetupForm()
{

}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::ComboBox1Change(TObject *Sender)
{
   K1->ModbusIndex=17501+100*ComboBox1->ItemIndex;
   Type1->ModbusIndex=13289+ComboBox1->ItemIndex;
   K1->Read();
   Type1->Read();
}
//---------------------------------------------------------------------------
void __fastcall TSetupForm::BitBtn1Click(TObject *Sender)
{
   RadioGroup2->Font->Color=clBlack;
   NumberEdit1->Font->Color=clBlack;
   NumberEdit2->Font->Color=clBlack;
   NumberEdit3->Font->Color=clBlack;
   Label2->Font->Color=clBlack;
   Label5->Font->Color=clBlack;
   float f;
   f=NumberEdit1->Text.ToDouble();
   K1->Float[0]=f;
   Write[0]=true;
   K1->Write();

   f=NumberEdit3->Text.ToDouble();
   K2->Float[0]=f;
   Write[1]=true;
   K2->Write();

   f=NumberEdit2->Text.ToDouble();
   V->Float[0]=f;
   Write[5]=true;
   V->Write();

   short sh;
   sh=RadioGroup2->ItemIndex;
   ProverType->Int16[0]=sh;
   Write[4]=true;
   ProverType->Write();

   sh=0;
   if(RadioButton1->Checked)
      sh=1;
   Type1->Int16[0]=sh;
   Write[2]=true;
   Type1->Write();

   sh=0;
   if(RadioButton3->Checked)
      sh=1;
   Type2->Int16[0]=sh;
   Write[3]=true;
   Type2->Write();

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#include <vcl.h>
//#include "Pch.H"
#pragma hdrstop

#include "CalculatePlenumPressForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TCalculatePlenumPressForm *CalculatePlenumPressForm;
//---------------------------------------------------------------------------
__fastcall TCalculatePlenumPressForm::TCalculatePlenumPressForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCalculatePlenumPressForm::NumberEdit1Change(
      TObject *Sender)
{
   try
      {
      double p,r,p1,pr;
      p=NumberEdit1->Text.ToDouble();
      r=NumberEdit2->Text.ToDouble();
      p1=NumberEdit3->Text.ToDouble();
      pr=(p/r)+p1;
      double m=1.05*pr;
      AnsiString s=FloatToStrF(pr,Sysutils::ffNumber,1000000,3);
      s+=" - (+5%) ";
      s+=FloatToStrF(m,Sysutils::ffNumber,1000000,3);
      Label4->Font->Color=clBlack;
      Label4->Caption=s;

      pr=pr*10.2;
      m=m*10.2;
      s=FloatToStrF(pr,Sysutils::ffNumber,1000000,2);
      s+=" - (+5%) ";
      s+=FloatToStrF(m,Sysutils::ffNumber,1000000,2);
      Label7->Font->Color=clBlack;
      Label7->Caption=s;


      }
   catch(...)
      {
      Label4->Font->Color=clRed;
      Label4->Caption="������!";
      Label7->Font->Color=clRed;
      Label7->Caption="������!";
      }
}
//---------------------------------------------------------------------------

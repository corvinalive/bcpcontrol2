object SetupForm: TSetupForm
  Left = 218
  Top = 248
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1086#1074#1077#1088#1082#1080
  ClientHeight = 200
  ClientWidth = 452
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 126
    Width = 61
    Height = 13
    Caption = #1054#1073#1098#1077#1084' '#1058#1055#1059
  end
  object RadioGroup2: TRadioGroup
    Left = 4
    Top = 6
    Width = 185
    Height = 115
    Caption = #1058#1080#1087' '#1055#1059
    ItemIndex = 2
    Items.Strings = (
      #1054#1076#1085#1086#1085#1072#1087#1088#1072#1074#1083#1077#1085#1085#1099#1081
      #1044#1074#1091#1085#1072#1087#1088#1072#1074#1083#1077#1085#1085#1099#1081
      'Compact Prover'
      #1044#1074#1091#1085#1072#1087#1088#1072#1074#1083#1077#1085#1085#1099#1081' - SVP'
      #1050#1086#1085#1090#1088#1086#1083#1100#1085#1099#1081' '#1055#1056)
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 198
    Top = 6
    Width = 123
    Height = 163
    Caption = #1055#1086#1074#1077#1088#1103#1077#1084#1099#1081' '#1055#1056
    TabOrder = 1
    object Label4: TLabel
      Left = 6
      Top = 50
      Width = 115
      Height = 29
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1103' '#1055#1056':'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 8
      Top = 108
      Width = 40
      Height = 13
      Caption = #1058#1080#1087' '#1055#1056':'
    end
    object ComboBox1: TComboBox
      Left = 6
      Top = 22
      Width = 111
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = #1055#1056' '#8470'1'
      OnChange = ComboBox1Change
      Items.Strings = (
        #1055#1056' '#8470'1'
        #1055#1056' '#8470'2'
        #1055#1056' '#8470'3'
        #1055#1056' '#8470'4')
    end
    object NumberEdit1: TNumberEdit
      Left = 8
      Top = 84
      Width = 109
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = '0'
      NumberType = Double
    end
    object RadioButton1: TRadioButton
      Left = 6
      Top = 120
      Width = 113
      Height = 17
      Caption = #1052#1072#1089#1089#1086#1084#1077#1088
      Checked = True
      TabOrder = 2
      TabStop = True
    end
    object RadioButton2: TRadioButton
      Left = 6
      Top = 140
      Width = 113
      Height = 17
      Caption = #1058#1055#1056
      TabOrder = 3
    end
  end
  object NumberEdit2: TNumberEdit
    Left = 6
    Top = 142
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    NumberType = Double
  end
  object GroupBox2: TGroupBox
    Left = 326
    Top = 6
    Width = 123
    Height = 163
    Caption = #1050#1086#1085#1090#1088#1086#1083#1100#1085#1099#1081' '#1055#1056
    TabOrder = 3
    object Label3: TLabel
      Left = 6
      Top = 50
      Width = 115
      Height = 29
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1103' '#1055#1056':'
      WordWrap = True
    end
    object Label5: TLabel
      Left = 8
      Top = 108
      Width = 40
      Height = 13
      Caption = #1058#1080#1087' '#1055#1056':'
    end
    object ComboBox2: TComboBox
      Left = 6
      Top = 22
      Width = 111
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      ItemIndex = 3
      TabOrder = 0
      Text = #1055#1056' '#8470'4'
      Items.Strings = (
        #1055#1056' '#8470'1'
        #1055#1056' '#8470'2'
        #1055#1056' '#8470'3'
        #1055#1056' '#8470'4')
    end
    object NumberEdit3: TNumberEdit
      Left = 8
      Top = 84
      Width = 109
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = '0'
      NumberType = Double
    end
    object RadioButton3: TRadioButton
      Left = 6
      Top = 120
      Width = 113
      Height = 17
      Caption = #1052#1072#1089#1089#1086#1084#1077#1088
      TabOrder = 2
    end
    object RadioButton4: TRadioButton
      Left = 6
      Top = 140
      Width = 113
      Height = 17
      Caption = #1058#1055#1056
      Checked = True
      TabOrder = 3
      TabStop = True
    end
  end
  object BitBtn1: TBitBtn
    Left = 2
    Top = 172
    Width = 193
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Default = True
    TabOrder = 4
    OnClick = BitBtn1Click
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 198
    Top = 172
    Width = 253
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 5
    Kind = bkClose
  end
end

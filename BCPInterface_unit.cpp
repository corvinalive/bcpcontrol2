//---------------------------------------------------------------------------


#pragma hdrstop

#include "BCPInterface_unit.h"
#include "BCPControlForm_unit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//static TSaveDataEvent* OnSave;

void __fastcall ShowBCPControl(TSaveDataEvent OnSave1)
{
   BCPControlForm->OnSave=OnSave1;
   BCPControlForm->Show();
}

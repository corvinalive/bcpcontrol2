object BCPControlForm: TBCPControlForm
  Left = 187
  Top = 248
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Brooks Compact Prover Control'
  ClientHeight = 305
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StateLabel: TLabel
    Left = 2
    Top = 206
    Width = 225
    Height = 17
    AutoSize = False
    Color = clSkyBlue
    ParentColor = False
    WordWrap = True
  end
  object Label1: TLabel
    Left = 4
    Top = 53
    Width = 87
    Height = 13
    Caption = #1055#1086#1074#1077#1088#1103#1077#1084#1099#1081' '#1055#1056':'
  end
  object Label4: TLabel
    Left = 2
    Top = 91
    Width = 109
    Height = 29
    Alignment = taCenter
    AutoSize = False
    Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1103' '#1055#1056':'
    WordWrap = True
  end
  object Label5: TLabel
    Left = 2
    Top = 121
    Width = 99
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 4
    Top = 135
    Width = 40
    Height = 13
    Caption = #1058#1080#1087' '#1055#1056':'
  end
  object Label7: TLabel
    Left = 52
    Top = 135
    Width = 53
    Height = 13
    Caption = #1052#1072#1089#1089#1086#1084#1077#1088
  end
  object UpstreamLabel: TLabel
    Left = 1
    Top = 18
    Width = 110
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Upstream'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object RunLabel: TLabel
    Left = 1
    Top = 36
    Width = 110
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Run'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 160
    Width = 42
    Height = 13
    Caption = #8470' '#1090#1086#1095#1082#1080
  end
  object Label14: TLabel
    Left = 107
    Top = 162
    Width = 118
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1080#1079#1084#1077#1088#1077#1085#1080#1081
  end
  object Label18: TLabel
    Left = 282
    Top = 162
    Width = 201
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1080#1079#1084#1077#1088#1077#1085#1080#1081' '#1076#1083#1103' '#1091#1089#1088#1077#1076#1085#1077#1085#1080#1103
  end
  object Label3: TLabel
    Left = 228
    Top = 204
    Width = 133
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = #1057#1074#1103#1079#1100' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1072
    Color = clLime
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object DDELabel: TLabel
    Left = 0
    Top = 0
    Width = 111
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = #1057#1074#1103#1079#1100' DDE'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object PageControl1: TPageControl
    Left = 113
    Top = 0
    Width = 432
    Height = 153
    ActivePage = TabSheet1
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1055#1086#1074#1077#1088#1082#1072' '#1087#1086' '#1058#1055#1059
      object GroupBox1: TGroupBox
        Left = 2
        Top = 2
        Width = 409
        Height = 117
        Caption = #1044#1072#1085#1085#1099#1077' '#1055#1056
        TabOrder = 0
        object Label8: TLabel
          Left = 4
          Top = 43
          Width = 71
          Height = 16
          Caption = #1056#1072#1089#1093#1086#1076', '#1090'/'#1095
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 4
          Top = 27
          Width = 70
          Height = 16
          Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label12: TLabel
          Left = 4
          Top = 11
          Width = 118
          Height = 16
          Caption = #1050#1086#1083'-'#1074#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object QLabel: TLabel
          Left = 126
          Top = 43
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DLabel: TLabel
          Left = 126
          Top = 27
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object NLabel: TLabel
          Left = 126
          Top = 11
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label13: TLabel
          Left = 4
          Top = 59
          Width = 80
          Height = 16
          Caption = #1056#1072#1089#1093#1086#1076', '#1084'3/'#1095
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Q3Label: TLabel
          Left = 126
          Top = 59
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 4
          Top = 75
          Width = 89
          Height = 16
          Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 4
          Top = 91
          Width = 65
          Height = 16
          Caption = #1044#1072#1074#1083#1077#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object tLabel: TLabel
          Left = 126
          Top = 75
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object PLabel: TLabel
          Left = 126
          Top = 91
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object NNLabel: TLabel
          Left = 220
          Top = 9
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 220
          Top = 43
          Width = 45
          Height = 16
          Caption = 'TDVOL'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label32: TLabel
          Left = 220
          Top = 59
          Width = 34
          Height = 16
          Caption = 'T imp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object T1Label: TLabel
          Left = 270
          Top = 43
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object T2Label: TLabel
          Left = 270
          Top = 59
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object PeriodLabel: TLabel
          Left = 220
          Top = 27
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 358
          Top = 56
          Width = 38
          Height = 13
          Caption = 'Label15'
        end
        object dpLabel: TLabel
          Left = 317
          Top = 27
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Button1: TButton
          Left = 204
          Top = 96
          Width = 199
          Height = 19
          Caption = #1044#1072#1074#1083#1077#1085#1080#1077' '#1074' '#1087#1085#1077#1074#1084#1086#1089#1080#1089#1090#1077#1084#1077'...'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1086#1074#1077#1088#1082#1072' '#1087#1086' '#1050#1058#1055#1056
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 2
        Top = 2
        Width = 409
        Height = 117
        Caption = #1044#1072#1085#1085#1099#1077' '#1055#1056
        TabOrder = 0
        object Label17: TLabel
          Left = 4
          Top = 43
          Width = 71
          Height = 16
          Caption = #1056#1072#1089#1093#1086#1076', '#1090'/'#1095
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 4
          Top = 27
          Width = 70
          Height = 16
          Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 4
          Top = 11
          Width = 118
          Height = 16
          Caption = #1050#1086#1083'-'#1074#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Q2Label: TLabel
          Left = 126
          Top = 43
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object D2Label: TLabel
          Left = 126
          Top = 27
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object N2Label: TLabel
          Left = 126
          Top = 11
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 4
          Top = 59
          Width = 80
          Height = 16
          Caption = #1056#1072#1089#1093#1086#1076', '#1084'3/'#1095
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Q32Label: TLabel
          Left = 126
          Top = 59
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 4
          Top = 75
          Width = 89
          Height = 16
          Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 4
          Top = 91
          Width = 65
          Height = 16
          Caption = #1044#1072#1074#1083#1077#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object t22Label: TLabel
          Left = 126
          Top = 75
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object P2Label: TLabel
          Left = 126
          Top = 91
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object V1Label: TLabel
          Left = 220
          Top = 59
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label35: TLabel
          Left = 220
          Top = 75
          Width = 140
          Height = 16
          Caption = #1054#1073#1098#1077#1084' '#1088#1072#1073#1086#1095#1077#1075#1086' '#1058#1055#1056
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object V2Label: TLabel
          Left = 220
          Top = 91
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label37: TLabel
          Left = 220
          Top = 43
          Width = 169
          Height = 16
          Caption = #1054#1073#1098#1077#1084' '#1082#1086#1085#1090#1088#1086#1083#1100#1085#1086#1075#1086' '#1058#1055#1056
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Period2Label: TLabel
          Left = 220
          Top = 28
          Width = 7
          Height = 16
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
  object SetupProveDataButton: TButton
    Left = 406
    Top = 180
    Width = 139
    Height = 25
    Caption = #1055#1086#1076#1075#1086#1090#1086#1074#1080#1090#1100' '#1076#1072#1085#1085#1099#1077'...'
    TabOrder = 1
    OnClick = SetupProveDataButtonClick
  end
  object GoProveButton: TButton
    Left = 2
    Top = 180
    Width = 137
    Height = 25
    Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
    TabOrder = 2
    OnClick = GoProveButtonClick
  end
  object ComboBox1: TComboBox
    Left = 2
    Top = 67
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 3
    Text = #1055#1056' '#8470'1'
    OnChange = ComboBox1Change
    Items.Strings = (
      #1055#1056' '#8470'1'
      #1055#1056' '#8470'2'
      #1055#1056' '#8470'3'
      #1055#1056' '#8470'4')
  end
  object PointNumberEdit: TCSpinEdit
    Left = 58
    Top = 156
    Width = 43
    Height = 22
    MaxValue = 99
    MinValue = 1
    TabOrder = 4
    Value = 1
  end
  object TotalRunEdit: TCSpinEdit
    Left = 231
    Top = 158
    Width = 43
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 5
    Value = 1
  end
  object AverageRunEdit: TCSpinEdit
    Left = 494
    Top = 160
    Width = 43
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 6
    Value = 1
  end
  object Button2: TButton
    Left = 140
    Top = 180
    Width = 83
    Height = 25
    Caption = #1055#1088#1077#1088#1074#1072#1090#1100
    TabOrder = 7
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 226
    Width = 547
    Height = 79
    Align = alBottom
    TabOrder = 8
  end
  object SelectVarButton: TButton
    Left = 360
    Top = 204
    Width = 191
    Height = 25
    Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1077#1076#1072#1074#1072#1077#1084#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081'...'
    TabOrder = 9
    OnClick = SelectVarButtonClick
  end
  object SendCurrentDataButton: TButton
    Left = 224
    Top = 182
    Width = 181
    Height = 25
    Caption = #1055#1077#1088#1077#1076#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1077' '#1076#1072#1085#1085#1099#1077
    TabOrder = 10
    OnClick = SendCurrentDataButtonClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = OnTimer
    Left = 334
    Top = 4
  end
end

//---------------------------------------------------------------------------

#include <vcl.h>
#include <IniFiles.hpp>
#pragma hdrstop

#include "DDEServer.h"
#include "BCP.h"
#include "BCPControlForm_unit.h"
#include "SetupForm_unit.h"
#include "CalculatePlenumPressForm_unit.h"
#include "SelectVarForm_unit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TBCPControlForm *BCPControlForm;
//---------------------------------------------------------------------------
__fastcall TBCPControlForm::TBCPControlForm(TComponent* Owner)
   : TForm(Owner)
{
   AverageRunEdit->MaxValue=MaxAverageRuns;
   CurrentTotalRun=0;
   CurrentAverageRun=0;

   TotalRuns=0;
   AverageRuns=0;
   
   ProveState=ProveNone;

   UsedVars.t=true;
   UsedVars.P=true;
   UsedVars.Period=true;
   UsedVars.N=true;
   TIniFile* ini = new TIniFile((ExtractFilePath(Application->ExeName)+"Omni.ini"));
   DensK[0] = ini->ReadFloat("Density","K0",0);
   DensK[1] = ini->ReadFloat("Density","K1",0);
   DensK[2] = ini->ReadFloat("Density","K2",0);
   DensK[3] = ini->ReadFloat("Density","K18",0);
   DensK[4] = ini->ReadFloat("Density","K19",0);
   DensK[5] = ini->ReadFloat("Density","K20A",0);
   DensK[6] = ini->ReadFloat("Density","K20B",0);
   DensK[7] = ini->ReadFloat("Density","K21A",0);
   DensK[8] = ini->ReadFloat("Density","K21B",0);
   delete ini;

   PRType = new TModbusPoint(0,TModbusPoint::int16,&OnTypeComm);
   PRType->AutoRead=false;
   K = new TModbusPoint(0,TModbusPoint::float32,&OnKComm);
   K->AutoRead=false;

   Q = new TModbusPoint(0,TModbusPoint::float32,&OnQComm);
   Q->AutoRead=false;

   Q3 = new TModbusPoint(0,TModbusPoint::float32,&OnCommQ3);
   Q3->AutoRead=false;

   P = new TModbusPoint(7904,TModbusPoint::float32,&OnCommP);
   P->AutoRead=false;

   t = new TModbusPoint(7901,TModbusPoint::float32,&OnCommt);
   t->AutoRead=false;

   D = new TModbusPoint(7912,TModbusPoint::float32,&OnCommD);
   D->AutoRead=false;

   N = new TModbusPoint(5901,TModbusPoint::int32,&OnCommN);
   N->AutoRead=false;
   T1 = new TModbusPoint(5974,TModbusPoint::int32,&OnCommT1,2);
   T1->AutoRead=false;

   NN = new TModbusPoint(8051,TModbusPoint::float32,&OnCommNN);
   NN->AutoRead=false;

   V1 = new TModbusPoint(8080,TModbusPoint::float32,&OnCommV1);
   V1->AutoRead=false;

   V2 = new TModbusPoint(8086,TModbusPoint::float32,&OnCommV2);
   V2->AutoRead=false;

   Period = new TModbusPoint(15134,TModbusPoint::int32,&OnCommPeriod);
   Period->AutoRead=false;

   Prove = new TModbusPoint(0,TModbusPoint::Boolean,&OnProve);
   Prove->AutoRead=false;

   ProveEndOk = new TModbusPoint(1911,TModbusPoint::Boolean,&OnProveOk);
   ProveEndError = new TModbusPoint(1912,TModbusPoint::Boolean,&OnProveError);

   Run = new TModbusPoint(1927,TModbusPoint::Boolean,&OnCommRun);
   Run->AutoRead=false;

   Upstream = new TModbusPoint(1731,TModbusPoint::Boolean,&OnCommUp);
   Upstream->AutoRead=false;



   CustomPacket = new TModbusPoint(01,TModbusPoint::Custom,&OnCommCustom);

   try
   {
   mb = new TModbus("Omni.ini");
   }
   catch(...){};
   mb->OnCommBreak=OnCommBreak;
   mb->OnCommRestore=OnCommOk;
   if(mb->CommOk)
      OnCommOk();
   else
      OnCommBreak();


   mb->AddNeedRead(PRType);
   mb->AddNeedRead(K);
   mb->AddNeedRead(Q);
   mb->AddNeedRead(Q3);
   mb->AddNeedRead(P);
   mb->AddNeedRead(t);
   mb->AddNeedRead(D);
   mb->AddNeedRead(N);
   mb->AddNeedRead(T1);
   mb->AddNeedRead(NN);
   mb->AddNeedRead(V1);
   mb->AddNeedRead(V2);
   mb->AddNeedRead(Prove);
   mb->AddNeedRead(ProveEndOk);
   mb->AddNeedRead(ProveEndError);

   mb->AddNeedRead(Run);
   mb->AddNeedRead(Upstream);

   mb->AddNeedRead(Period);

   mb->AddNeedRead(CustomPacket);
   ComboBox1Change(0);
   mb->Resume();

   Server.Init("ServiceBCP","TopicBCP","ItemBCP");
   Server.OnAdviseLoop=OnLoop;
   Timer1->Enabled=true;
}
//---------------------------------------------------------------------------
__fastcall TBCPControlForm::~TBCPControlForm()
{
   Timer1->Enabled=false;
   mb->Stop=true;
   mb->Terminate();
   mb->WaitFor();
   while(mb->Stoped==false)
      {};

   delete mb;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommBreak()
{
   Label3->Caption="��� �����";
   Label3->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommOk()
{
   Label3->Caption="����� �����������";
   Label3->Color=clLime;
   //Read
   PRType->Read();
   K->Read();
   Q->Read();
   Q3->Read();
   P->Read();
   t->Read();
   D->Read();
   N->Read();
   T1->Read();
   NN->Read();
   V1->Read();
   V2->Read();
   ProveEndOk->Read();
   ProveEndError->Read();
   Run->Read();
   Upstream->Read();
   Period->Read();
   CustomPacket->Read();
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::ComboBox1Change(TObject *Sender)
{
   //Get type of ��
//13289 - �� �1 0-��������, 1 - ��������
//13290 - �� �2 0-��������, 1 - ��������
//13291 - �� �3 0-��������, 1 - ��������
//13292 - �� �4 0-��������, 1 - ��������
   PRType->ModbusIndex=13289+ComboBox1->ItemIndex;
   PRType->Read();
   K->ModbusIndex=17501+100*ComboBox1->ItemIndex;
   K->Read();

   Q->ModbusIndex=7103+100*ComboBox1->ItemIndex;
   Q->Read();

   Q3->ModbusIndex=7101+100*ComboBox1->ItemIndex;
   Q3->Read();

   Prove->ModbusIndex=1714+ComboBox1->ItemIndex;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnTypeComm()
{
   if(PRType->Int16[0])
      Label7->Caption="��������";
   else
      Label7->Caption="���";
   if(PRType->State==TModbusPoint::CommOk)
      Label7->Font->Color=clBlue;
   else
      Label7->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnKComm()
{
   Label5->Caption=K->Float[0];
   if(K->State==TModbusPoint::CommOk)
      Label5->Font->Color=clBlue;
   else
      Label5->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnQComm()
{
   QLabel->Caption=FloatToStrF(Q->Float[0] ,Sysutils::ffNumber,1000000, 2);
   Q2Label->Caption=FloatToStrF(Q->Float[0] ,Sysutils::ffNumber,1000000, 2);
   CurrentData.Q=Q->Float[0];
   if(ProveState==ReadData)
      {
      ReadedVars.Q=true;
      CurrentRun.Q=Q->Float[0];
      }
   if(Q->State==TModbusPoint::CommOk)
      {
      QLabel->Font->Color=clBlue;
      Q2Label->Font->Color=clBlue;
      }
   else
      {
      QLabel->Font->Color=clRed;
      Q2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommQ3()
{
   Q3Label->Caption=FloatToStrF(Q3->Float[0] ,Sysutils::ffNumber,1000000, 2);
   Q32Label->Caption=FloatToStrF(Q3->Float[0] ,Sysutils::ffNumber,1000000, 2);
   CurrentData.Q3=Q3->Float[0];
   if(ProveState==ReadData)
      {
      ReadedVars.Q3=true;
      CurrentRun.Q3=Q3->Float[0];
      }
   if(Q3->State==TModbusPoint::CommOk)
      {
      Q3Label->Font->Color=clBlue;
      Q32Label->Font->Color=clBlue;
      }
   else
      {
      Q3Label->Font->Color=clRed;
      Q32Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommt()
{
   tLabel->Caption=FloatToStrF(t->Float[0] , Sysutils::ffNumber,1000000, 2);
   t22Label->Caption=FloatToStrF(t->Float[0] ,Sysutils::ffNumber,1000000, 2);
   CurrentData.t=t->Float[0];
   dens_t = t->Float[0];
   if(ProveState==ReadData)
      {
      ReadedVars.t=true;
      CurrentRun.t=t->Float[0];
      }
   if(t->State==TModbusPoint::CommOk)
      {
      tLabel->Font->Color=clBlue;
      t22Label->Font->Color=clBlue;
      }
   else
      {
      tLabel->Font->Color=clRed;
      t22Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommP()
{
   pp=P->Float[0]/1000;
   dens_p = pp;
   PLabel->Caption=FloatToStrF(pp ,Sysutils::ffNumber,1000000, 3);
   P2Label->Caption=FloatToStrF(pp ,Sysutils::ffNumber,1000000, 3);
   CurrentData.P=pp;
   if(ProveState==ReadData)
      {
      CurrentRun.P=pp;
      ReadedVars.P=true;
      }
   if(P->State==TModbusPoint::CommOk)
      {
      PLabel->Font->Color=clBlue;
      P2Label->Font->Color=clBlue;
      }
   else
      {
      PLabel->Font->Color=clRed;
      P2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommD()
{
   DLabel->Caption=FloatToStrF(D->Float[0] ,Sysutils::ffNumber,1000000, 3);
   D2Label->Caption=FloatToStrF(D->Float[0] ,Sysutils::ffNumber,1000000, 3);
   CurrentData.Dens=D->Float[0];
   if(ProveState==ReadData)
      {
      CurrentRun.Dens=D->Float[0];
      ReadedVars.Dens=true;
      }
   if(D->State==TModbusPoint::CommOk)
      {
      DLabel->Font->Color=clBlue;
      D2Label->Font->Color=clBlue;
      }
   else
      {
      DLabel->Font->Color=clRed;
      D2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommPeriod()
{
   double p=Period->Int32[0];
   p=p/1000;
   PeriodLabel->Caption=FloatToStrF(p ,Sysutils::ffNumber,1000000, 3);
   Period2Label->Caption=FloatToStrF(p ,Sysutils::ffNumber,1000000, 3);
   CurrentData.Period=p;
   if(DensK[0] != 0)
      {
      double cd = CalculateDensity(p,dens_t,dens_p);
      dpLabel->Caption=FloatToStrF(cd ,Sysutils::ffNumber,1000000, 2);
      }
   if(ProveState==ReadData)
      {
      CurrentRun.Period=p;
      ReadedVars.Period=true;
//      Log("Save period");
      }
   if(Period->State==TModbusPoint::CommOk)
      {
      PeriodLabel->Font->Color=clBlue;
      Period2Label->Font->Color=clBlue;
      }
   else
      {
      PeriodLabel->Font->Color=clRed;
      Period2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommN()
{
   NLabel->Caption=N->Int32[0];
   N2Label->Caption=N->Int32[0];
   CurrentData.N=N->Int32[0];
   if(ProveState==ReadData)
      {
      CurrentRun.N=N->Int32[0];
      ReadedVars.N=true;
      }
   if(N->State==TModbusPoint::CommOk)
      {
      NLabel->Font->Color=clBlue;
      N2Label->Font->Color=clBlue;
      }
   else
      {
      NLabel->Font->Color=clRed;
      N2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommNN()
{
   NNLabel->Caption=FloatToStrF(NN->Float[0],Sysutils::ffNumber,1000000, 3);
   CurrentData.NN=NN->Float[0];
   if(ProveState==ReadData)
      {
      CurrentRun.NN=NN->Float[0];
      ReadedVars.NN=true;
      }
   if(NN->State==TModbusPoint::CommOk)
      NNLabel->Font->Color=clBlue;
   else
      NNLabel->Font->Color=clRed;
   static int counter=0;
   counter++;
   Label15->Caption=counter;

}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::SetupProveDataButtonClick(TObject *Sender)
{
   SetupForm->Setup(mb);
   ComboBox1Change(0);
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnTimer(TObject *Sender)
{
   if(ProveState==ReadData)
      { //   Sleep(10000);
      if(CheckForReaded())
         {
         ProveState=SimpleRunEnd;
//         Log("End read");
         OnSimpleRunEnd();
         }
      }
   if(ProveState==NeedReadData)
      {
//      Log("Begin to read");
      ClearReadedList();
      ProveState=ReadData;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::GoProveButtonClick(TObject *Sender)
{
   TotalRuns=TotalRunEdit->Value;
   AverageRuns=AverageRunEdit->Value;
   CurrentTotalRun=0;
   CurrentAverageRun=0;

   GoProve();
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnProve()
{
   if(Prove->State==TModbusPoint::CommOk)
      {
      StateLabel->Caption="��������� ��������";
      ProveState=ProveInProgres;
      Sleep(1000);
      ProveEndOk->Read();
      ProveEndError->Read();
      }
   else
      {
      StateLabel->Caption="��������� �� ������� ���������";
      ProveState=ProveError;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnProveOk()
{
   if(ProveState==ProveInProgres)
      {
      if(ProveEndOk->State!=TModbusPoint::CommOk)
         {//������ ������ - ����������
         ProveEndOk->Read();
         return;
         }
      if(ProveEndOk->Bool[0]==true)
         {
         StateLabel->Caption="��������� ��������� �������. ������ ������";
         ProveState=NeedReadData;
         }
      else
         {//��������� ��� �� ���������. ����������
         ProveEndOk->Read();
         }
      }

}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnProveError()
{
   if(ProveState==ProveInProgres)
      {
      if(ProveEndError->State!=TModbusPoint::CommOk)
         {//������ ������ - ����������
         ProveEndError->Read();
         return;
         }
      if(ProveEndError->Bool[0]==true)
         {
         StateLabel->Caption="��������� ��������� ��������";
         ProveState=ProveError;
         }
      else
         {//��������� ��� �� ���������. ����������
         ProveEndError->Read();
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommV1()
{
   V1Label->Caption=V1->Float[0];
   CurrentData.V_etalon=V1->Float[0];
   if(ProveState==ReadData)
      {
      ReadedVars.V_etalon=true;
      CurrentRun.V_etalon=V1->Float[0];
      }
   if(V1->State==TModbusPoint::CommOk)
      V1Label->Font->Color=clBlue;
   else
      V1Label->Font->Color=clRed;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommV2()
{
   try
   {
   V2Label->Caption=V2->Float[0];
   CurrentData.V_work=V2->Float[0];
   if(ProveState==ReadData)
      {
      CurrentRun.V_work=V2->Float[0];
      ReadedVars.V_work=true;
      }
   if(V2->State==TModbusPoint::CommOk)
      V2Label->Font->Color=clBlue;
   else
      V2Label->Font->Color=clRed;
   }
   catch(...){};
}
//---------------------------------------------------------------------------
double TBCPControlForm::CalculateDensity(double Period,double t,double P)
{
// t        - �����������
// �        - ��������, ���
// Period   - ������ ������� ��, �����
//D= K0 + K1 * T + K2 * T^2
      double D=DensK[0] + DensK[1] * Period + DensK[2] * Period * Period;
//Dt=D * [1 + K18 * (t - 20) ] + K19 * (t - 20)
      double Dt = D*(1 + DensK[3] * (t - 20)) + DensK[4] * (t - 20);
      //K20 = K20A + K20B * P(bar);
      //10 - ��� �������� �������� � ����
      double K20=DensK[5] + DensK[6] * P * 10;
      //K21 = K21A + K21B * P(bar);
      //10 - ��� �������� �������� � ����
      double K21=DensK[7] + DensK[8] * P * 10;
      //DtP=Dt * [1 + (K20 * P)] + K21 * P;
      D=Dt * (1 + (K20 * P * 10)) + K21 * P * 10;
   return D;
}

void __fastcall TBCPControlForm::Button1Click(TObject *Sender)
{
   CalculatePlenumPressForm->NumberEdit2->Text=3.2;
   CalculatePlenumPressForm->NumberEdit3->Text=0.414;
   CalculatePlenumPressForm->Show();
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommT1()
{
   double t1=T1->Int32[0];
   t1=t1/1000000;
   double t2=T1->Int32[1];
   t2=t2/1000000;
   CurrentData.Time=t1;
   CurrentData.Time2=t2;
   if(ProveState==ReadData)
      {
      CurrentRun.Time=t1;
      CurrentRun.Time2=t2;
      ReadedVars.Time=true;
      ReadedVars.Time2=true;
      }
   T1Label->Caption=FloatToStrF(t1,Sysutils::ffNumber,1000000, 3);
   T2Label->Caption=FloatToStrF(t2,Sysutils::ffNumber,1000000, 3);
   if(T1->State==TModbusPoint::CommOk)
      {
      T1Label->Font->Color=clBlue;
      T2Label->Font->Color=clBlue;
      }
   else
      {
      T1Label->Font->Color=clRed;
      T2Label->Font->Color=clRed;
      }
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommRun()
{
   if((Run->State==TModbusPoint::CommOk) && (Run->Bool[0]==false))
      RunLabel->Color=clYellow;
   else
      RunLabel->Color=clBtnFace;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommUp()
{
   if((Upstream->State==TModbusPoint::CommOk) && (Upstream->Bool[0]==false))
      UpstreamLabel->Color=clYellow;
   else
      UpstreamLabel->Color=clBtnFace;
}
//---------------------------------------------------------------------------
/*
TODO:
13/08/2006
������:
1. �������������� ���������� ��������� ���������� ��������� � ��������
   ���������� ������ � �� 2463-98 (� � ������ ���������).
���������:
1.0. ���� ������ (��������) �������������:
     - ���������� ���������;
     - ���������� ��������� ��� ����������;
     - ����� ������������ ������ ������������;
1.1. ���������� ���������� ���������;
1.2. ���������� ����������� (��� �������������);
1.3. �������� ������ ���������;
������ ������� � ������� �����������:
1. ���� ����������� � ���� DLL, �������:
1.1 ������������ �������, ������� ���������� ���� BCP Control, ��� ����������
    ��������� ���������� call-back �������
1.2. ������������ �������, ������� ������� ������ ������ TBCPControl, �������
     ������������� ��� ���������������� � ������� �������, ������� � �������.
2. � ������� ����� � ���� ������� � ������� OPC
�������:
1. ��� ����������, � ����� ����� ��������� ���������� ������?
   ��������:   ������ 1.1.
   1. ���������� � BCP Control ����� �����. ����� - ����������� ����������
   ��������� ������ � ������������ �����.
   2. �������� ID �����. ����� - ���������.
2. ��� ��������� ���������?
   ��������:   ������ 2.1.
   2.1. � BCP Control
   ����� - �� ���� ������������ ���������� ���������, ����� �������������;
   2.2. � ���������� ����������
   ����� - ������� �� �-���������� - ������� �� �������� ��� �� ����, ���
   ������������ �������-������.
3. ��� ��������, ����� ������ ����� ��������������?
   ��������:
   3.1. � BCP Control
   3.2. � ���������� ���������
   ��������� - ��� ������������ ����� ����������:
   1. �����, �� ������� ����� ������ ���� ��������� ������. � ���� ������ �����
   ����� ����������� ����������� ����������� ��������� ������ - �� ����,
   � �����������

*/

//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnSimpleRunEnd()
{
   //Save Prove Data
   Runs[CurrentAverageRun]=CurrentRun;
   CurrentAverageRun++;
   AnsiString s;
   s="��������� �";
   s+=(CurrentTotalRun+1);
   s+="/";
   s+=CurrentAverageRun;
   s+=" ���������";
   Log(s);
   //TODO: save data of prove to log
   s="NN=";
   s+=CurrentRun.NN;
   s+=" N=";
   s+=CurrentRun.N;
   s+=" Period=";
   s+=CurrentRun.Period;
   s+=" Dens=";
   s+=CurrentRun.Dens;
   s+=" Time=";
   s+=CurrentRun.Time;
   s+="t=";
   s+=CurrentRun.t;
   s+=" P=";
   s+=CurrentRun.P;
   Log(s);
   if(CurrentAverageRun==AverageRuns)
      {
      CalculateAverage();
      //TODO: ��������� ���������. �������� ������
      SendData(&Runs[MaxAverageRuns],&UsedVars);

      CurrentAverageRun=0;
      CurrentTotalRun++;
      if(CurrentTotalRun==TotalRuns)
         {
         s="���������� ";
         s+=TotalRuns;
         s+="-� ��������� ���������. ������� ��������:";
         Log(s);
         s="NN=";
         s+=FloatToStrF(Runs[MaxAverageRuns].NN,Sysutils::ffNumber,1000000, 3);
         s+=" ������=";
         s+=FloatToStrF(Runs[MaxAverageRuns].Period,Sysutils::ffNumber,1000000, 3);
         s+="  t=";
         s+=FloatToStrF(Runs[MaxAverageRuns].t,Sysutils::ffNumber,1000000, 3);;
         s+="  P=";
         s+=FloatToStrF(Runs[MaxAverageRuns].P,Sysutils::ffNumber,1000000, 3);
         s+="   Dens=";
         s+=FloatToStrF(Runs[MaxAverageRuns].Dens,Sysutils::ffNumber,1000000, 3);;
         Log(s);
         }
      else
         {
         GoProve();
         }
      }
   else
      {
      GoProve();
      }
}
//---------------------------------------------------------------------------

void __fastcall TBCPControlForm::Button2Click(TObject *Sender)
{
   ProveState=ProveNone;
   StateLabel->Caption="��������� �������� �������������";
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::SelectVarButtonClick(TObject *Sender)
{
   SelectVarForm->tBox->Checked=UsedVars.t;
   SelectVarForm->PBox->Checked=UsedVars.P;
   SelectVarForm->DBox->Checked=UsedVars.Dens;
   SelectVarForm->PeriodBox->Checked=UsedVars.Period;
   SelectVarForm->Time1Box->Checked=UsedVars.Time;
   SelectVarForm->Time2Box->Checked=UsedVars.Time2;
   SelectVarForm->NBox->Checked=UsedVars.N;
   SelectVarForm->NNBox->Checked=UsedVars.NN;
   SelectVarForm->QBox->Checked=UsedVars.Q;
   SelectVarForm->Q3Box->Checked=UsedVars.Q3;
   SelectVarForm->V1Box->Checked=UsedVars.V_etalon;
   SelectVarForm->V2Box->Checked=UsedVars.V_work;

   if(SelectVarForm->ShowModal()==mrOk)
      {
      UsedVars.t=SelectVarForm->tBox->Checked;
      UsedVars.P=SelectVarForm->PBox->Checked;
      UsedVars.Dens=SelectVarForm->DBox->Checked;
      UsedVars.Period=SelectVarForm->PeriodBox->Checked;
      UsedVars.Time=SelectVarForm->Time1Box->Checked;
      UsedVars.Time2=SelectVarForm->Time2Box->Checked;
      UsedVars.N=SelectVarForm->NBox->Checked;
      UsedVars.NN=SelectVarForm->NNBox->Checked;
      UsedVars.Q=SelectVarForm->QBox->Checked;
      UsedVars.Q3=SelectVarForm->Q3Box->Checked;
      UsedVars.V_etalon=SelectVarForm->V1Box->Checked;
      UsedVars.V_work=SelectVarForm->V2Box->Checked;
       }

}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::GoProve()
{
   Prove->Bool[0]=true;
   Prove->Write();
   ProveState=WriteCommandStart;
   StateLabel->Caption="������ ���������";
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::CalculateAverage()
{
   memset(&Runs[MaxAverageRuns],0,sizeof(TProveData));
   for(int i=0;i<AverageRuns;i++)
      {
      Runs[MaxAverageRuns].t+=Runs[i].t;
      Runs[MaxAverageRuns].P+=Runs[i].P;
      Runs[MaxAverageRuns].Dens+=Runs[i].Dens;
      Runs[MaxAverageRuns].Period+=Runs[i].Period;
      Runs[MaxAverageRuns].Time+=Runs[i].Time;
      Runs[MaxAverageRuns].Time2+=Runs[i].Time2;
      Runs[MaxAverageRuns].N+=Runs[i].N;
      Runs[MaxAverageRuns].NN+=Runs[i].NN;
      Runs[MaxAverageRuns].Q+=Runs[i].Q;
      Runs[MaxAverageRuns].Q3+=Runs[i].Q3;
      Runs[MaxAverageRuns].V_etalon+=Runs[i].V_etalon;
      Runs[MaxAverageRuns].V_work+=Runs[i].V_work;
      }
   Runs[MaxAverageRuns].t/=AverageRuns;
   Runs[MaxAverageRuns].P/=AverageRuns;
   Runs[MaxAverageRuns].Dens/=AverageRuns;
   Runs[MaxAverageRuns].Period/=AverageRuns;
   Runs[MaxAverageRuns].Time/=AverageRuns;
   Runs[MaxAverageRuns].Time2/=AverageRuns;
   Runs[MaxAverageRuns].N/=AverageRuns;
   Runs[MaxAverageRuns].NN/=AverageRuns;
   Runs[MaxAverageRuns].Q/=AverageRuns;
   Runs[MaxAverageRuns].Q3/=AverageRuns;
   Runs[MaxAverageRuns].V_etalon/=AverageRuns;
   Runs[MaxAverageRuns].V_work/=AverageRuns;

}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::Log(AnsiString Msg)
{
   Memo1->Lines->Add(Msg);
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::ClearReadedList()
{
    ReadedVars.t=false;
    ReadedVars.P=false;
    ReadedVars.Dens=false;
    ReadedVars.Period=false;
    ReadedVars.Time=false;
    ReadedVars.Time2=false;
    ReadedVars.N=false;
    ReadedVars.NN=false;
    ReadedVars.Q=false;
    ReadedVars.Q3=false;
    ReadedVars.V_etalon=false;
    ReadedVars.V_work=false;
}
//---------------------------------------------------------------------------
bool __fastcall TBCPControlForm::CheckForReaded()
{
    if(ReadedVars.t==false) return false;
    if(ReadedVars.P==false) return false;;
    if(ReadedVars.Dens==false) return false;;
    if(ReadedVars.Period==false) return false;;
    if(ReadedVars.Time==false) return false;;
    if(ReadedVars.Time2==false) return false;;
    if(ReadedVars.N==false) return false;;
    if(ReadedVars.NN==false) return false;;
    if(ReadedVars.Q==false) return false;;
    if(ReadedVars.Q3==false) return false;;
    if(ReadedVars.V_etalon==false) return false;;
    if(ReadedVars.V_work==false) return false;
    return true;
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnLoop(TDDEServer *Sender)
{
   if(Sender->HasLoop())
      DDELabel->Color=clLime;
   else
      DDELabel->Color=clRed;
};
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::SendCurrentDataButtonClick(TObject *Sender)
{
   SendData(&CurrentData,&UsedVars);
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::SendData(TProveData* Data, TUsedVars* Used)
{
   AnsiString s=Data->DataToString(Used);
   int Point=PointNumberEdit->Value;
   if(Point<10)//���� ����
      {
      s+='0';
      s+=Point;
      }
   else
      {
      if(Point>99)
         Point=99;
      s+=Point;
      }
   Server.OnDataChange(s.c_str(),s.Length());
}
//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::OnCommCustom()
{
   //Mirror and copy data to ModbusPoints and call event handlers
   char* current_element=CustomPacket->Data;
   //Q
   CopyDataFromCustomToPoint(current_element,Q,4);
   current_element+=4;
   //Q3
   CopyDataFromCustomToPoint(current_element,Q3,4);
   current_element+=4;
   //P
   CopyDataFromCustomToPoint(current_element,P,4);
   current_element+=4;
   //t
   CopyDataFromCustomToPoint(current_element,t,4);
   current_element+=4;
   //D
   CopyDataFromCustomToPoint(current_element,D,4);
   current_element+=4;
   //N
   CopyDataFromCustomToPoint(current_element,N,4);
   current_element+=4;
   //T1 and T2
   CopyDataFromCustomToPoint(current_element,T1,4,2);
   current_element+=8;
   //NN
   CopyDataFromCustomToPoint(current_element,NN,4);
   current_element+=4;
   //V1
   CopyDataFromCustomToPoint(current_element,V1,4);
   current_element+=4;
   //V2
   CopyDataFromCustomToPoint(current_element,V2,4);
   current_element+=4;
   //Period
   CopyDataFromCustomToPoint(current_element,Period,4);
   current_element+=4;
   CopyDataFromCustomToPoint(current_element,Run,1);
   current_element++;
   CopyDataFromCustomToPoint(current_element,Upstream,1);
  // current_element++;
}

//---------------------------------------------------------------------------
void __fastcall TBCPControlForm::CopyDataFromCustomToPoint(char* From, TModbusPoint* To, int size,int count)
{
   //TODO: Add your source code here
   CustomPacket->Mirror(From,size);
   memcpy(To->Data,From,size);
   if(count==2)
      {
      char* c=From+size;
      char* tc=To->Data+size;
      CustomPacket->Mirror(c,size);
      memcpy(tc,c,size);
      }
   To->State=CustomPacket->State;
   To->OnComm();
}
//---------------------------------------------------------------------------

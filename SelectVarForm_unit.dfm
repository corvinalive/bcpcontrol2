object SelectVarForm: TSelectVarForm
  Left = 229
  Top = 148
  Width = 393
  Height = 288
  Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1077#1076#1072#1074#1072#1077#1084#1099#1093' '#1087#1077#1088#1077#1084#1077#1085#1085#1099#1093
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object tBox: TCheckBox
    Left = 4
    Top = 4
    Width = 380
    Height = 17
    Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072', '#1086#1057
    TabOrder = 0
  end
  object PBox: TCheckBox
    Left = 4
    Top = 22
    Width = 380
    Height = 17
    Caption = #1044#1072#1074#1083#1077#1085#1080#1077', '#1052#1055#1072
    TabOrder = 1
  end
  object DBox: TCheckBox
    Left = 4
    Top = 40
    Width = 380
    Height = 17
    Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100', '#1082#1075'/'#1084'3'
    TabOrder = 2
  end
  object PeriodBox: TCheckBox
    Left = 4
    Top = 58
    Width = 380
    Height = 17
    Caption = #1055#1077#1088#1080#1086#1076' '#1089#1080#1075#1085#1072#1083#1072' '#1087#1083#1086#1090#1085#1086#1084#1077#1088#1072', '#1084#1082#1089
    TabOrder = 3
  end
  object Q3Box: TCheckBox
    Left = 4
    Top = 76
    Width = 380
    Height = 17
    Caption = #1056#1072#1089#1093#1086#1076' '#1055#1056', '#1084'3/'#1095
    TabOrder = 4
  end
  object QBox: TCheckBox
    Left = 4
    Top = 94
    Width = 380
    Height = 17
    Caption = #1056#1072#1089#1093#1086#1076' '#1055#1056', '#1090'/'#1095
    TabOrder = 5
  end
  object Time1Box: TCheckBox
    Left = 4
    Top = 112
    Width = 380
    Height = 17
    Caption = #1042#1088#1077#1084#1103' '#1087#1088#1086#1093#1086#1078#1076#1077#1085#1080#1103' '#1087#1086#1088#1096#1085#1103', '#1089#1077#1082
    TabOrder = 6
  end
  object Time2Box: TCheckBox
    Left = 4
    Top = 130
    Width = 380
    Height = 17
    Caption = #1042#1088#1077#1084#1103' '#1084#1077#1078#1076#1091' '#1087#1077#1088#1077#1076#1085#1080#1084#1080' '#1092#1088#1086#1085#1090#1072#1084#1080' 1-'#1075#1086' '#1080' '#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074', '#1089#1077#1082
    TabOrder = 7
  end
  object NBox: TCheckBox
    Left = 4
    Top = 148
    Width = 380
    Height = 17
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074' ('#1094#1077#1083#1099#1077')'
    TabOrder = 8
  end
  object NNBox: TCheckBox
    Left = 4
    Top = 166
    Width = 380
    Height = 17
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074' '#1089' '#1076#1086#1083#1103#1084#1080
    TabOrder = 9
  end
  object V1Box: TCheckBox
    Left = 4
    Top = 184
    Width = 380
    Height = 17
    Caption = #1054#1073#1098#1077#1084' '#1078#1080#1076#1082#1086#1089#1090#1080', '#1087#1088#1086#1096#1077#1076#1096#1080#1081' '#1095#1077#1088#1077#1079' '#1101#1090#1072#1083#1086#1085#1085#1099#1081' '#1055#1056
    TabOrder = 10
  end
  object V2Box: TCheckBox
    Left = 4
    Top = 202
    Width = 380
    Height = 17
    Caption = #1054#1073#1098#1077#1084' '#1078#1080#1076#1082#1086#1089#1090#1080', '#1087#1088#1086#1096#1077#1076#1096#1080#1081' '#1095#1077#1088#1077#1079' '#1087#1086#1074#1077#1088#1103#1077#1084#1099#1081' '#1055#1056
    TabOrder = 11
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 224
    Width = 180
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 12
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 196
    Top = 224
    Width = 180
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 13
    Kind = bkCancel
  end
end

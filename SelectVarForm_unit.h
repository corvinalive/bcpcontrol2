//---------------------------------------------------------------------------

#ifndef SelectVarForm_unitH
#define SelectVarForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TSelectVarForm : public TForm
{
__published:	// IDE-managed Components
   TCheckBox *tBox;
   TCheckBox *PBox;
   TCheckBox *DBox;
   TCheckBox *PeriodBox;
   TCheckBox *Q3Box;
   TCheckBox *QBox;
   TCheckBox *Time1Box;
   TCheckBox *Time2Box;
   TCheckBox *NBox;
   TCheckBox *NNBox;
   TCheckBox *V1Box;
   TCheckBox *V2Box;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
private:	// User declarations
public:		// User declarations
   __fastcall TSelectVarForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSelectVarForm *SelectVarForm;
//---------------------------------------------------------------------------
#endif
